char b;
#include <eHealth.h>
#include <PinChangeInt.h>
int cont = 0;
// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);  
//  pinMode(13, OUTPUT);
   eHealth.initPulsioximeter();

  //Attach the inttruptions for using the pulsioximeter.   
  PCintPort::attachInterrupt(6, readPulsioximeter, RISING);

}

// the loop routine runs over and over again forever:
void loop() {
  int temperature = eHealth.getTemperature();

  //Serial.print("Temperature (ºC): ");       

  if(Serial.available())
    b=Serial.read();
  else
    b='\0';
    
  if(b=='a')
    Serial.print(temperature);
    
  if(b=='p')
     Serial.print(eHealth.getOxygenSaturation());
  //  digitalWrite(13, HIGH);
      
    int air = eHealth.getAirFlow();   

  if(b=='m')
    eHealth.airFlowWave(air);  


}

void readPulsioximeter(){  

  cont ++;

  if (cont == 50) { //Get only of one 50 measures to reduce the latency
    eHealth.readPulsioximeter();  
    cont = 0;
  }
}


