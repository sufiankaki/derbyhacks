import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity;

// http://hc.apache.org/httpcomponents-client-ga/tutorial/html/fundamentals.html  -  Apache HttpClient
// https://forum.processing.org/topic/http-post-processing  -  Integration with Processing (different from above)
// https://code.google.com/p/processing/source/browse/trunk/processing/java/libraries/net/examples/HTTPClient/HTTPClient.pde?r=7950  -  Messy.
// https://github.com/francisli/processing-http  -  Unnecessary?

// This code assumes the presence of the following Apache HTTPClient JARs added to the sketch's 'code' directory:
//   - httpclient-4.2.3.jar
//   - httpcore-4.2.2.jar
//   - commons-logging-1.1.1.jar

// This can be accomplished by manually copying, or by using 'Add File' in the Processing IDE.
// It does _not_ work to add these JARs to the sketchbook/libraries directory for some unknown reason.

String data;
PFont font;
HttpClient client = new DefaultHttpClient();
String url = "https://api.syncano.io/v1.1/instances/pulldata/classes/telehealth/objects/";
HttpPost method = new HttpPost(url);
HttpResponse response = null;
HttpEntity entity = null;

void setup() {
  size(800,600);
  background(50);
  font = createFont("Helvetica", 10, true);
  textFont(font);  
 
 JSONObject json=new JSONObject();
  json.setString("channel", "telehealthchannel");
  json.setString("DeviceID", "DRBTst121");
  json.setInt("Temperature", 31);   //Integer.parseInt(temp)
  json.setInt("Pulse", 75);
  json.setInt("Breath", 12);
  
  try{
   method.setHeader("X-API-KEY", "7b72b1f8a0b00ad6bb8bf6f8c6dbf0107ef155c6");
  method.setHeader("Content-Type", "application/json");
    
 method.setEntity(new StringEntity(json.toString()));
  }
  
  catch(Exception E)
  {
    
    
  }
  println("executing request: " + method.getRequestLine());
  
  try {
     response = client.execute(method);
     entity = response.getEntity();
     // OR: entity.writeTo(System.out);
     // OR: InputStream instream = entity.getContent();
     if(null != entity)
       text(EntityUtils.toString(entity), 5, 15);
  } catch(Exception e) {
     
  } finally {
    client.getConnectionManager().shutdown();     
  }
  
  
  
}