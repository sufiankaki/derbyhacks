import processing.serial.*;
import http.requests.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity;

Serial myPort;  // Create object from Serial class
int val;      // Data received from the serial port
String temp;
int butX, butY, butWidth = 150, butLength = 50;
color butColor, butHighlight, currentColor;
boolean butOver = false;
JSONObject json;
PImage image;
PImage airflow;
PImage bodypos;
PImage bp;
PImage ecg;
PImage emg;
PImage gluco;
PImage gsr;
PImage spo2;
PFont f;

//test
HttpClient client = new DefaultHttpClient();
String url = "https://api.syncano.io/v1.1/instances/pulldata/classes/telehealth/objects/";
HttpPost method = new HttpPost(url);
HttpResponse response = null;
HttpEntity entity = null;

void setup() 
{
  size(1200, 800);
  initialize();
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
 image = loadImage("temp.png");
 airflow = loadImage("airflow.png");
 bodypos = loadImage("bodypos.png");
 bp = loadImage("bp.png");
 ecg = loadImage("ecg.png");
 emg = loadImage("emg.png");
 gluco = loadImage("gluco.png");
 gsr = loadImage("gsr.png");
 spo2 = loadImage("spo2.png");
 
 background(currentColor);
  image(image, 100, 100, 150, 150);
   image(airflow, 350, 100, 150, 150);
    image(bodypos, 600, 100, 150, 150);
     image(bp, 850, 100, 150, 150);
      image(ecg, 350, 350, 150, 150);
       image(emg, 600, 350, 150, 150);
        image(gluco, 850, 350, 150, 150);
         image(gsr, 100, 350, 150, 150);
         
         printArray(PFont.list());
  f = createFont("SourceCodePro-Regular.ttf", 40);
  textFont(f);
  textAlign(CENTER);
         text("HackMyHealth.com", 600, 60);
  fill(51);
  
  
 }


void draw()
{
  
          //image(spo2, 600, 600, 150, 150);
         
  update_mouse();
  initialize();
  if(butOver)
  {
    fill(butHighlight);
  }
  else
    fill(butColor);
  stroke(0);
  rect(butX, butY, butWidth, butLength);
   f = createFont("SourceCodePro-Regular.ttf", 24);
  fill(51);
         text("Submit", width/2, 690);
  
}

void initialize()
{
  butColor = color(255);
  butHighlight = color(234);
  currentColor = color(192);
  butX = width/2-butWidth/2;
  butY = 650;
  
   }


void update_mouse()
{
  if(overButton(butX, butY, butWidth, butLength))
    butOver = true;
  else
    butOver = false;
}

void mousePressed()
{
  if(butOver)
    execute();
}

void execute()
{
  myPort.write('a');
  delay(2000);
  json = new JSONObject();
  temp = "";
  while ( myPort.available()>0 ) {  // If data is available,
   val = myPort.read();     // read it and store it in val
   temp = temp + char(val);
  }
  
  
  
  //int tempint = Integer.parseInt(temp);
  json.setString("channel", "telehealthchannel");
  json.setString("DeviceID", "DRBTst121");
  json.setString("Temperature", temp);   //Integer.parseInt(temp)
  
  myPort.write('p');
  delay(2000);
  temp = "";
  while ( myPort.available()>0 ) {  // If data is available,
   val = myPort.read();     // read it and store it in val
   temp = temp + char(val);
  }
  json.setString("Pulse", temp);
  
  myPort.write('m');
  delay(2000);
  temp = "";
  while ( myPort.available()>0 ) {  // If data is available,
   val = myPort.read();     // read it and store it in val
   temp = temp + char(val);
  }
  json.setString("Breath", temp);
 
 
  
  //temp = str(temp, 0, 2);
  System.out.println(temp);
  
  
  
  
  
  
  try{
   method.setHeader("X-API-KEY", "7b72b1f8a0b00ad6bb8bf6f8c6dbf0107ef155c6");
  method.setHeader("Content-Type", "application/json");
    
 method.setEntity(new StringEntity(json.toString()));
  }
  
  catch(Exception E)
  {
    
    
  }
  println("executing request: " + method.getRequestLine());
  
  try {
     response = client.execute(method);
     entity = response.getEntity();
     // OR: entity.writeTo(System.out);
     // OR: InputStream instream = entity.getContent();
     if(null != entity)
      println(EntityUtils.toString(entity));
  } catch(Exception e) {
     
  } finally {
    client.getConnectionManager().shutdown();     
  }
  f = createFont("SourceCodePro-Regular.ttf", 24);
  fill(51);
         text("Posted!", width/2 + 250, 690);
  
  
}

boolean overButton(int x, int y, int wid, int hgt)
{
  if(mouseX >= x && mouseX <= x+wid && mouseY >= y && mouseY <= y+hgt)
    return true;
  else
    return false;
}